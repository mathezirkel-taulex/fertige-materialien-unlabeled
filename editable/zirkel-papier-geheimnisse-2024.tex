% Hier den Pfad zu zirkel.cls angeben
\documentclass{../../tex-bilder/zirkel}

\graphicspath{{../../tex-bilder/}}

\usepackage{mdframed}
\usepackage{array}      % for spacing in tabular

\renewcommand{\schuljahr}{2023/24}
\renewcommand{\klasse}{9}
\renewcommand{\titel}{Geheimnisse des A4-Papiers}
\renewcommand{\untertitel}{Präsenzzirkel vom 11. April 2024}
\renewcommand{\name}{Alexander Mai}

% Vordefinierte Umgebungen
%     * aufgabe
%     * hinweis
%     * loesung
%     * satz
%     * beispiel

\begin{document}
    \makeheaderunlabeled

    \vspace*{-2em}

    \section{Verhältnis des A4-Papiers}
    
    Eine besondere Eigenschaft von Papier im A4-Format ist, dass wir es längs halbieren können und dadurch zwei A5-Papiere herausbekommen.
    
    \begin{figure}[h!]
        \centering
        \includegraphics[width=0.7\linewidth]{papier/a4-to-a5.pdf}
    \end{figure}

    Das gleiche gilt auch für alle anderen A-Papiere: halbiert man sie längs, bekommt man das nächstkleinere Format, mit jeweils einer Nummer größer, bis auf Fehler im Bereich von einem Millimeter. Besonders ist auch, dass alle Papiere der A-Familie das gleiche Verhältnis haben. Dass das nach der Halbierung wirklich der Fall ist, liegt am ganz besonderen Verhältnis des A-Formats.

    \begin{aufgabe}[Verhältnismäßig besonderes Verhältnis]
        Berechne das Seitenverhältnis von Papieren im A-Format. % sqrt(2), oder das Inverse davon, also (1 / sqrt(2)), je nach Reihenfolge, in der man das Verhältnis sieht. Berechnung am besten mit einer Zeichnung wie aus der oberen Abbildung machen, in der man dann sieht, dass man einfach folgende Gleichung auflösen muss:
        % 2a / b = b / a    | mult mit a und mit b
        % 2aa = bb          | "sqrt"
        % sqrt(2) a = b
    \end{aufgabe}

    \enlargethispage{3.5\baselineskip}

    Obwohl es viele andere Papierformate gibt, können wir uns glücklich schätzen, dass wir uns in weiten Teilen Europas auf Papiere im A-Format geeinigt haben. In anderen Teilen der Welt gibt es teilweise ganz andere Konventionen. Die folgende Grafik zeigt die üblichsten Papierformate in den USA. Die Angabe der Seitenlängen ist hier in Zoll (englisch: Inch), wobei ein Zoll genau 2,54cm entspricht.

    \begin{figure}[h!]
        \centering
        \includegraphics[width=\linewidth]{papier/paper-sizes.pdf}
    \end{figure}

    \begin{aufgabe}[Verhältnismäßg praktisch]
        Überlege dir, welche praktischen Vorteile im Alltag entstehen durch die Standardgröße von A4-Papier und die anderen Größen der A-Familie. % einfach bisschen Brainstorming mit den Kindern. Hier ein paar Ideen:
        % - weniger Sortiererei und Müll durch übrigbleibende einzelne Größen von Papier und allem, was dazugehört
        % - einheitliche Briefumschläge (ob dreier-gefaltet, oder ohne Faltung)
        % - einheitliche Ordner- und Mappen- und Schnellheftergrößen, die man für alles von Schule bis Juristerei einsetzen kann
        % - ...
        % - 
        % - 

        \textbf{Bonus:} Finde Beispiele für Papiere im Alltag, die nicht im A-Format sind. % Postkarten, Taschentücher, Küchenpapier, Notizzettel und Post-Its, viele Bücher (bzw deren Seiten), Arztrezepte, Kassenzettel, ...
    \end{aufgabe}


    \section{Maße des A4-Papiers}
    
    Neben dem Verhältnis ist noch die genaue Größe relevant.
    A-Papier orientiert sich an der Maßeinheit des \emph{Meters}.
    Genauer hat A0-Papier eine Fläche von einem Quadratmeter, wobei die Seitenlängen dann auf den Millimeter gerundet werden.
    Für die nächstkleineren A-Papiere werden halbe Millimeter immer abgerundet.

    \begin{aufgabe}[Verhältnismäßig vermessen]
        \vspace*{-1em}
        \begin{enumerate}[a)]
            \item Wie viele Quadratmillimeter passen in einen Quadratmeter? % 1.000.000 viele, weil 1.000 Millimeter in einen Meter passen und damit diese Zahl quadriert für Quadratmillimeter im Quadratmeter
            \item Wie groß sind die Seitenlängen eines A0-Papiers? % 841mm x 1.189mm, ergibt eine Fläche von 999.949 mm^2. Danach A1: 594mm x 841mm. A2: 420mm x 594mm. A3: 297mm x 420mm. A4: 210mm x 297mm.
            \item Wie groß sind die Seitenlängen eines A4-Papiers? % 210mm x 297mm
        \end{enumerate}
    \end{aufgabe}


    \section{Rätselgeheimnis des A4-Papiers}
    

    Aus einem A4-Papier kann man ganz leicht ein Quadrat bekommen, indem man eine Ecke so in die gegenüberliegende lange Seite legt, dass die kurze Kante dieser Ecke genau auf dieser langen Kante liegt und man den entstehenden Knick ordentlich faltet.

    \begin{aufgabe}[Verhältnismäßig vielseitig]
        Wo verbirgt sich nun das Quadrat? Und warum funktioniert dieser Trick überhaupt? % die erste Falte ist die Diagonale vom Quadrat. Das funktioniert bei jedem rechteckigen Papier, weil wir dabei einen rechten Winkel des Papiers halbieren und diese halbierende Kante auf eine der Papierkanten auch im 45°-Winkel eintrifft. Dadurch kriegen wir zunächst die Hälfte eines Quadrats, welche aber umgefaltet genau auf der anderen Hälfte des Quadrats liegt, ohne überzustehen.
    \end{aufgabe}

    Mit einer weiteren ähnlichen Faltung entsteht eine Form wie im folgenden Bild.

    \enlargethispage{2.5\baselineskip}

    \begin{figure}[h!]
        \centering
        \includegraphics[width=0.85\linewidth]{papier/a4-paper-puzzle.pdf}
    \end{figure}

    \begin{aufgabe}[Verhältnismäßig knobelig]
        \label{ex:mattparkera4puzzle}
        Berechne den Umfang dieser neuen Form. % 4. Beziehungsweise 4 mal die kurze Seite vom A4-Papier, also 4 * 210mm. Für die Rechnung einfach Schritt für Schritt Seitenlängen voneinander abziehen und Pythagoras nutzen, zB für die Länge der langen Diagonale, sqrt(2), und für die Länge der kurzen Diagonale, sqrt(2) * (sqrt(2) - 1). Die Länge vom kleinen übrigbleibenden Rechteck ist dann noch 1 - (sqrt(2) - 1), also 2 - sqrt(2). Damit haben wir insgesamt unten, lange Diagonale, kurze Diagonale, kleines-Rechteck-Länge: sqrt(2) + sqrt(2) + sqrt(2) * (sqrt(2) - 1) + 2 - sqrt(2) und das lässt sich vereinfachen zu 2 + sqrt(2) * (1 + 1 + sqrt(2) - 1 - 1) und das ist dann 2 + sqrt(2) * sqrt(2) = 2 + 2 = 4.
    \end{aufgabe}

    
    \newpage


    \section{Irrationalität des A4-Papiers}

    In Aufgabe~\ref{ex:mattparkera4puzzle} haben wir den Umfang einer kurios gefalteten Form berechnet. Interessant ist nun das Rechteck, das dabei unten rechts entsteht.

    \begin{aufgabe}[Verhältnismäßig verhältnismäßig]
        Berechne das Seitenverhältnis des übrig gebliebenen Rechtecks. % es ist wieder sqrt(2). Nutze dafür Seitenlängen aus der vorherigen Aufgabe um zu sehen, dass die Seitenlängen vom kleinen übrigen Rechteck sind: sqrt(2) - 1 und 2 - sqrt(2). Das als Bruch schreiben und bisschen umformen, zB beides multiplizieren mit (2 + sqrt(2)) für den Bruch (sqrt(2) / 2) = 1 / sqrt(2).
    \end{aufgabe}
    
    Dieses Verhältnis hat fantastische Konsequenzen. Eine ist, dass wir nun einen Irrationalitätsbeweis fast ohne weitere Vorarbeit führen könnten. Dieser Beweis wird erst klarer nach einem Zirkel zu Quadratspiralen. Bis dahin betrachten wir das hier nur als eine Bonusaufgabe:

    \begin{aufgabe}[Verhältnismäßig bewiesen]
        Beweise: $\sqrt{2}$ ist eine irrationale Zahl. Das heißt:
        $$\nexists p \in \mathbb{Z} , \, \, q \in \mathbb{N}^+ : \, \frac{p}{q} = \sqrt{2} $$ % Siehe Zirkel zu Quadratspiralen. Im Grunde lässt sich jetzt auch in das kleine übrig gebliebene Rechteck genau so zwei Quadrate reinlegen und dann kommt ein noch kleineres übriges Rechteck heraus, aber wieder mit dem Verhältnis sqrt(2). Und das geht immer weiter. Damit geht ein einfacher Widerspruchsbeweis, der zeigt, dass wenn sqrt(2) eine rationale Zahl wäre, es irgendwann ein Ende der einsetzbaren Quadrate geben müsste. Das gibt es aber nicht, wie wir bereits gezeigt haben => sqrt(2) ist irrational.
    \end{aufgabe}


    \section{Geheime Graphstruktur des zerknautschten Papiers}
    
    Wenn man ein Papier zusammenknüllt und dann noch plattdrückt, zum Beispiel auf einer Tischplatte, können wir einige fantastische mathematische und physikalische Phänomene beobachten. So ein zerknülltes und geplättetes Papier nennen wir ab jetzt \emph{zerknautscht}.

    \begin{satz}\label{th:evenedges}
        Faltet man ein zerknautschtes Papier wieder auf, so bilden die Ecken und Kanten der Zerknautschung einen Graphen, bei dem aus jeder Ecke immer gerade viele Kanten ausgehen.
    \end{satz}
    
    Den Beweis verschieben wir auf später. Hier ist ein Bild aus einer Anleitung für einen Origami-Löwen. In solchen Anleitungen können wir zumindest beispielhaft den Satz~\ref{th:evenedges} verifizieren.

    \enlargethispage{3.5\baselineskip}

    \begin{figure}[h!]
        \centering
        \includegraphics[width=\linewidth]{papier/origami.pdf}
    \end{figure}


    \newpage

    \vspace*{-4em}


    \section{Winkelgeheimnisse des zerknautschten Papiers}

    Wenn wir mitten in ein Papier eine Ecke hineinfalten, welche auch platt gedrückt werden kann, dann hat diese mindestens vier ausgehende Kanten.

    \begin{satz}\label{th:4edgesangles}
        Für jede mögliche plattdrückbare Ecke mit vier Kanten und den rundum beschrifteten Winkeln $\alpha, \beta, \gamma, \delta ,$ gilt:
        \begin{enumerate}
            \item $\alpha + \beta + \gamma + \delta = 360^\circ$
            \item $\alpha + \gamma = \beta + \delta$
        \end{enumerate}
    \end{satz}

    \begin{figure}[h!]
        \centering
        \includegraphics[width=0.8\linewidth]{papier/angles.pdf}
        \caption{Links eine mögliche plattgedrückte Ecke, daneben zwei unmögliche.}\label{fig:angles}
    \end{figure}

    Tatsächlich gilt sogar eine Verallgemeinerung. Erinnere dich dabei auch an Satz~\ref{th:evenedges}.

    \begin{satz}\label{th:2nedgesangles}
        Für jede mögliche plattdrückbare Ecke mit beliebig gerade vielen Kanten und daraus resultierenden, rundum beschrifteten Winkeln $\alpha_1, \alpha_2, \alpha_3, \dots, \alpha_{2n}$ gilt:
        \begin{enumerate}
            \item $\sum \limits_{i = 1}^{2n} \alpha_i = \alpha_1 + \alpha_2 + \alpha_3 + \dots + \alpha_{2n} = 360^\circ$
            \item $\sum \limits_{i = 1}^{2n} -(-1)^i \alpha_i = + \alpha_1 - \alpha_2 + \alpha_3 - \alpha_4 + \dots + \alpha_{2n - 1} - \alpha_{2n} = 0$
        \end{enumerate}
    \end{satz}

    \begin{aufgabe}
        \vspace*{-1em}
        \begin{enumerate}[a)]
            \item Beweise Satz~\ref{th:4edgesangles}. % einfach ein Papier mal so falten und dann sieht man es, wenn man genau hinschaut und nicht durcheinander kommt. Im Endeffekt wechseln sich die Winkel immer ab, ob sie in die eine oder in die andere Richtung gehen. Und am Ende muss man wieder am Anfang ankommen.
            \item Beweise Satz~\ref{th:2nedgesangles}. % gleiches Argument, wie beim vorherigen.
            \item Beweise Satz~\ref{th:evenedges}. % das folgt auch aus den vorherigen Überlegungen. Winkel wechseln sich in ihrer Richtung immer ab, sonst wäre keine Kante dazwischen.
        \end{enumerate}
    \end{aufgabe}


    \section{Geheime Physik und Kunst des zerknautschten Papiers}

    \enlargethispage{3.5\baselineskip}

    Zum Ende zwei kleine aber trotzdem atemberaubende Erkenntnisse zu zerknautschtem Papier:
    \begin{itemize}
        \item Zerknautschtes Papier hat eine negative \emph{Poissonzahl} (bzw. \emph{Querdehnungszahl}).
        \item Durch die Sätze~\ref{th:evenedges}, \ref{th:4edgesangles} und~\ref{th:2nedgesangles} ist es extrem schwer, ein aufgefaltetes zerknautschtes Papier realistisch aussehend zu malen.
    \end{itemize}

\end{document}